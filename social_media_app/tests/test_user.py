from fastapi import FastAPI
import requests
import pytest



# client = TestClient(app)

# def test_root():
#     response=requests.get("http://127.0.0.1:8000/api/testing")
#     assert response.status_code == 200
#     assert response.json() == {"message": "Running"}
    
testdata = [
    ('1',0,1,200,0,1),
    ('1',1,1,200,1,1),
    ('1',0,2,200,0,2),

    # ('1',0,2,422,"ensure this value is greater than 0","value_error.number.not_gt"),
    # (0,1,200,0,1),
    # (0,5,200,0,5),
    # (1,5,200,1,5),
    # (0,10,200,0,10),
    # (2,5,200,2,5),
]


data_post=[('1',
{
  "profile_id": "555",
  "user": {
    "fname": "string",
    "lname": "string"
  },
  "media": {
    "bannerImg": "string",
    "profileImg": "string"
  }
},
200
)]

data_delete=[("1","555",200)]

@pytest.mark.parametrize("logged_in_user_id,p,s,status,skip,limit",testdata)
def test_get_all_user(logged_in_user_id,p,s,status,skip,limit):
    response =  requests.get("http://127.0.0.1:8000/api/people/"+logged_in_user_id+"?skip="+str(p)+"&limit="+str(s))

    assert response.status_code==status
    
    if response.json()["code"]==200:
      assert response.json()["code"]==status
      assert response.json()["message"]=="success"


    if status==422:
        assert response.json()['detail'][0]['msg']==p
        assert response.json()['detail'][0]['type']==s
    else:
      da=response.json()["data"]
      if (skip==0 and limit==1):
          assert len(da)==1
      elif (skip==1 and limit==1):
          assert len(da)==1
      elif (skip==0 and limit==2):
          assert len(da)==2


@pytest.mark.parametrize("follow_user_id,json_data,status",data_post)
def test_follow(follow_user_id,json_data,status):
    assert len(follow_user_id)!=0

    url="http://127.0.0.1:8000/api/following/"+follow_user_id
    response=requests.post(url, json =json_data)
    try:
      assert response.json()['code']==status
    except:
      try:
        assert response.json()['code']==404
      except:
        assert response.json()['code']==409
    
    
    
@pytest.mark.parametrize("follow_user_id,following_profile_id,status",data_delete)
def test_unfollow(follow_user_id,following_profile_id,status):
    assert len(follow_user_id)!=0
    assert len(following_profile_id)!=0
    url="http://127.0.0.1:8000/api/unfollow/"+follow_user_id+"/"+following_profile_id
    response=requests.delete(url)
    try:
      assert response.json()['code']==status
    except:
      assert response.json()['code']==404
      
