from typing import Optional,List
from fastapi import FastAPI
from pydantic import BaseModel,Field

def ResponseModel(code,message,data):
   res={
    "code":code,
    "message":message,
    "data":data
    }
   return res
    
def ErrorResponseModel(code,message,error):
   res={
    "code":code,
    "message":message,
    "error":error
    }
   return res
