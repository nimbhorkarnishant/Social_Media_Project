from typing import Optional
from pydantic import BaseModel,Field



class User(BaseModel):
    profile_id:str
    user:dict={}
    media:dict={}
    following:list=[]
    follow:Optional[bool] = False


  
class user_profile(BaseModel):
    fname:Optional[str]=None
    lname:Optional[str]=None
         
class user_media(BaseModel):
    bannerImg:Optional[str]=None
    profileImg:Optional[str]=None
         
class Follow_people(BaseModel):
    profile_id:str
    user:Optional[user_profile]
    media:Optional[user_media]
       

