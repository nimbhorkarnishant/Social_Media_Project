from ..core.config import database_name, user_collection


async def read_db_sort_desc(conn,skip,limit):
    try:
        data= conn[database_name][user_collection].find().skip(skip).limit(limit)
        return data              #returning data if operation done successfully
    except Exception as exp:
        return str(exp)    



async def follow_people_db(conn,follow_user_id,json_data,op):
    try:
        if op=="$pull":
            data= await conn[database_name][user_collection].find_one({"profile_id":follow_user_id,"following.profile_id":json_data['profile_id']})
            if (data):
                await conn[database_name][user_collection].update_one({"profile_id":follow_user_id},{op:{"following":json_data}})
                return True          #returning true if db operation done successfully
            else:
                return data

        else:
            data= await conn[database_name][user_collection].find_one({"profile_id":follow_user_id})
            if (data):
                data1=await conn[database_name][user_collection].find_one({"profile_id":follow_user_id,"following.profile_id":json_data['profile_id']})

                if (data1):
                    return "followed"
                else:
                    await conn[database_name][user_collection].update_one({"profile_id":follow_user_id},{op:{"following":json_data}})
                    return True           #returning true if db operation done successfully
            else:
                return data

    except Exception as exp:
        return str(exp)    