from typing import Optional
from fastapi import APIRouter, Body, Depends, Path, Query
from bson.objectid import ObjectId
from starlette.exceptions import HTTPException
from starlette.status import (
    HTTP_201_CREATED,
    HTTP_204_NO_CONTENT,
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_422_UNPROCESSABLE_ENTITY,
)
from ....db.mongodb import AsyncIOMotorClient,get_database
from ....model.user import User,Follow_people
from ....model.response import *
from fastapi.encoders import jsonable_encoder

from ....crud.user import *
from ....redis_cache.redis import *
import pickle


router = APIRouter()



@router.get("/people/{logged_in_user_id}",tags=["Follow-Unfollow"])
async def get_all_user(logged_in_user_id:str,skip:int=0,limit:int=100,conn: AsyncIOMotorClient = Depends(get_database)):
    try:
        key=str(skip)+logged_in_user_id+str(limit)
        ########### checking data in cache
        data_cache=get_cache_data(key)       
        if (data_cache):
            ############# returning from cache
            return ResponseModel(200,"success",pickle.loads(data_cache))
        else:
            arr=[]
            data=await read_db_sort_desc(conn,skip,limit)
            if (data):
                try:
                    async for document in data:
                        arr.append(User(**document))
                        
                    for i in arr:
                        flag=False
                        for j in i.following:
                            if len(j)>0:
                                if j["profile_id"]==logged_in_user_id:
                                    flag=True
                                    break
                        i.follow=flag

                    ################# converting to string
                    c_data=pickle.dumps(arr)
                    ############ adding data to cache
                    set_cache_data(key,1200,c_data)
            
                    return ResponseModel(200,"success",arr)
                except Exception as exp:
                    return ErrorResponseModel(500,"Error",data)
            else:
                return ErrorResponseModel(500,"Error","Data Not Found with this Id")

    except Exception as exp:
        return ErrorResponseModel(500,"Error",str(exp))



@router.post("/following/{follow_user_id}",tags=["Follow-Unfollow"])
async def follow_people(user_data:Follow_people,follow_user_id:str,conn: AsyncIOMotorClient = Depends(get_database)):
    try:
        json_data = jsonable_encoder(user_data)
        print(json_data)
        data=await follow_people_db(conn,follow_user_id,json_data,"$push")
        if (data==True):
            delete_cache_data()
            return ResponseModel(200,"success",{"profile_id":follow_user_id})

        elif (data==None):
            return ErrorResponseModel(404,"Error","Data Not Found with this Id")
        
        elif (data=="followed"):
            return ErrorResponseModel(409,"Error","User with this Id Already followed")

        else:
            return ErrorResponseModel(500,"Error",data)
    except Exception as exp:
        return ErrorResponseModel(500,"Error",str(exp))
        


@router.delete("/unfollow/{follow_user_id}/{following_profile_id}",tags=["Follow-Unfollow"])
async def unfollow_people(following_profile_id:str,follow_user_id:str,conn: AsyncIOMotorClient = Depends(get_database)):
    try:
        json_data = {"profile_id":following_profile_id}
        print(json_data)
        data=await follow_people_db(conn,follow_user_id,json_data,"$pull")
        if (data==True):
            delete_cache_data()
            return ResponseModel(200,"success",[])

        elif (data==None):
            return ErrorResponseModel(404,"Error","Data Not Found with this Id")

        else:
            return ErrorResponseModel(500,"Error",data)
    except Exception as exp:
        return ErrorResponseModel(500,"Error",str(exp))




